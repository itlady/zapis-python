#!/usr/bin env python3

# ZÁKLADNÍ ŠKOLA ČAPKOVA
nazev_zs = "Základní škola Klatovy"
ulice_zs = "Čapkova ul. 126"
psc_zs = "339 01"
mesto_zs = "Klatovy"
reditel_zs = "PaedDr. Mgr. Dana Martinková, Ph.D."
tel_zs = "376 313 353"
email_zs = 	"zscapkova@investtel.cz"
www_zs = "https://www.zscapkova.cz"
ds_zs = "j8zgnrp"

