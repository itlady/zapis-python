#!/usr/bin env python3
#-*- coding: utf-8 -*-

# Lucie Názrová
# AIK2
# UDSK 2 - Python 1

import csv
import operator
from tkinter import *
from tkinter import messagebox
from tkinter import ttk
from tkinter.ttk import Combobox
from tkinter.ttk import *
from docx import Document
import docx
from docxtpl import DocxTemplate	# pip install docxtpl
from tkcalendar import *	#pip install tkcalendar
from datetime import date
from jinja2 import Environment, FileSystemLoader

import zs0
import zs1
import zs2
import zs3


# proměnné
rok = ("2022/2023")
zakladni_skola = ['Čapkova', 'Masarykova', 'Plánická', 'Tolstého']
font = 'Helvetica'
font_size1 = 30
font_size2 = 14
font_size3 = 12
akt_rok = ""
datum_ulozeno = False


# proměnné pro gui
ikona = "Ikona.ico"
title_root = "Aplikace ZÁPIS"

template1 = "template1.docx"
template2 = "template2.html"
output1 = "Oznámení.docx"
output2 = "prehled.html"
db = "databaze.csv"


#calendar
current = date.today()
current_year = int(current.strftime("%Y"))
current_month = int(current.strftime("%m"))
current_day = int(current.strftime("%d"))


# funkce
def vyber_skoly(event):
	global zs, title_main
	zs = combo1.get()
	title_main = title_root + " |  Základní škola " + zs
	return zs

def aktualni_rok(event):
	global combo2
	akt_rok = combo2.get()

def volba(_zs):
	global skola
	if zs == zakladni_skola[0]:
		skola = zs0
	elif zs == zakladni_skola[1]:
		skola = zs1
	elif zs == zakladni_skola[2]:
		skola = zs2
	elif zs == zakladni_skola[3]:
		skola = zs3
	return skola	

def zpracuj_datum():
	global den_zapisu, mesic_zapisu, rok_zapisu, datum_ulozeno, termin
	datum_zapisu = cal.get_date()
	datum_split = datum_zapisu.split("-")
	den_zapisu = datum_split[0]
	mesic_zapisu = datum_split[1]
	rok_zapisu = datum_split[2]
	text = "Zvolené datum je " + str(den_zapisu) + ". " + str(mesic_zapisu) + ". " + str(rok_zapisu) + "."
	label4.config(text=text)
	termin = "dne " + str(den_zapisu) + ". " + str(mesic_zapisu)  + ". "+ str(rok_zapisu) + " v době od 8 do 17 hodin"
	datum_ulozeno = True

# messagebox	
def showinfo():
	messagebox.showinfo('Hotovo', "Soubor uložen do složky programu")


def nahraj_db():
	global data
	with open(db, newline='', encoding='utf-8') as csvfile:
		data = csv.reader(csvfile, delimiter=';')
		# řazení tabulky podle příjmení vzestupně
		data = sorted(data, key=operator.itemgetter(2))
		

# docx template
def generuj_dokument():
	tpl = DocxTemplate(template1)
	ctx = {
		"nazev_zs": volba(zs).nazev_zs,
		"ulice_zs": volba(zs).ulice_zs,
		"psc_zs": volba(zs).psc_zs,
		"mesto_zs": volba(zs).mesto_zs,
		"akt_rok": akt_rok,
		"reditel_zs": volba(zs).reditel_zs,
		"email_zs": volba(zs).email_zs,
		"www_zs": volba(zs).www_zs,
		"tel_zs": volba(zs).tel_zs,
		"ds_zs": volba(zs).ds_zs,
		"termin": termin
	}
	tpl.render(ctx)
	tpl.save(output1)

	showinfo()

def generuj_prehled():
	env = Environment(loader=FileSystemLoader(''))
	tpl = env.get_template(template2)
	table_data = []
	for row in data:
		if zs in row[5]:
			table_data.append(row)

	html = tpl.render(page_title_text = "Přehled dětí",
					title_text = "Základní škola " + zs,
					headings=sloupce,
					data=table_data)

	with open(output2, 'w') as f:
		f.write(html)

	showinfo()


# GUI
def over_combo1():
	if combo1.get() == "":
		messagebox.showinfo ('Chyba', "Vyber základní školu z nabídky.")
	else: okno_programu()

def pokracuj():
	over_combo1()

def over_termin():
	if datum_ulozeno == False:
		messagebox.showinfo ('Chyba', "Nejdřív vyber datum.")
	else: generuj_dokument()


def okno_programu():
	global combo2, cal, label4 
	main = Toplevel()
	main.title(title_main)	
	main.geometry("820x820")
	main.resizable(0,0)
	main.iconbitmap(ikona)

	label3 = Label(main, text="Školní rok: ")
	label3.config(font=(font, font_size3))
	label3.pack(side=TOP, pady=(20,5))

	combo2 = Combobox(main, state="readonly")
	combo2['values'] = rok	
	combo2.bind("<<ComboboxSelected>>", aktualni_rok)
	combo2.current(0)
	combo2.pack(side=TOP)
	
	label5 = Label (main, text="Vyber datum zápisu:")
	label5.pack(padx=(0,600), pady=(25,5))

	cal = Calendar(main, selectmode="day", date_pattern="dd-mm-yyyy", year=current_year, month=current_month, day=current_day)
	cal.pack(fill="x", padx=20)
	
	btn4 = Button(main, text="Ulož datum zápisu", width=25, command=zpracuj_datum)
	btn4.pack(pady=(30,0))
		
	label4 = Label(main, text="")
	label4.pack(pady=15)

	btn2 = Button(main, text="Tisk oznámení", width=25, command=over_termin)
	btn2.pack(pady=5)
	
	f1 = LabelFrame(main)
	f1.pack(pady=10, padx=20)

	global sloupce
	sloupce = ('id', 'jméno', 'příjmení', 'bydliště', 'datum narození', 'základní škola')
	tree = ttk.Treeview(f1, columns=sloupce, show='headings', height=10)
	tree['columns'] = ('#1', '#2', '#3', '#4', '#5', '#6')
	tree.column('#1', width=50, minwidth=50, stretch=NO)
	tree.column('#2', width=110, minwidth=110, stretch=NO)
	tree.column('#3', width=155, minwidth=155, stretch=NO)
	tree.column('#4', width=155, minwidth=155, stretch=NO)
	tree.column('#5', width=125, minwidth=125, stretch=NO)
	tree.column('#6', width=155, minwidth=155, stretch=NO)

	tree.heading('#1', text="id", anchor=W)
	tree.heading('#2', text="Jméno", anchor=W)
	tree.heading('#3', text="Příjmení", anchor=W)
	tree.heading('#4', text="Bydliště", anchor=W)
	tree.heading('#5', text="Datum narození", anchor=W)
	tree.heading('#6', text="Základni škola", anchor=W)
	tree.pack(side='left')

	sbar = ttk.Scrollbar(f1, orient='vertical', command=tree.yview)
	sbar.pack(side='right', fill='y')
	tree.configure(yscrollcommand=sbar.set)
	
	nahraj_db()
	for row in data:
		# zobrazí děti pouze pro vybranou školu
		if zs in row[5]:
			global only_zs
			only_zs = row
			tree.insert('', END, values=(only_zs[0], only_zs[1], only_zs[2], only_zs[3], only_zs[4], only_zs[5]))
			global item_count
			item_count = len(tree.get_children())
	
	label6 = Label(main, text="Počet evidovaných dětí: \t" + str(item_count) + " / " + str(len(data)))
	label6.pack(pady=(2,15))
	btn5 = Button(main, text="Tisk přehledu dětí", width=25, command=generuj_prehled)
	btn5.pack(pady=5)


# MAIN
root = Tk()
root.title(title_root)
root.geometry("500x250")
root.resizable(0,0)
root.iconbitmap(ikona)

label1 = Label(root, text="ZÁPIS DO 1. TŘÍDY")
label1.config(font=(font, font_size1))
label1.pack(pady=(35,5))

label2 = Label(root, text="Volba základní školy:")
label2.config(font=(font, font_size2))
label2.pack(pady=(20,0))

global combo1
combo1 = Combobox(root, state="readonly")
combo1['values']=zakladni_skola
combo1.bind("<<ComboboxSelected>>", vyber_skoly)
combo1.pack(pady=15)

btn1 = Button(root, text="Pokračuj", width="12", command=pokracuj)
btn1.pack()

root.mainloop()


