#!/usr/bin env python3

# ZÁKLADNÍ ŠKOLA TOLSTÉHO
nazev_zs = "Základní škola Klatovy"
ulice_zs = "Tolstého 765"
psc_zs = "339 01"
mesto_zs = "Klatovy"
reditel_zs = "Mgr. Vítězslav Šklebený"
tel_zs = "376 312 352"
email_zs = "skola@zstolsteho.cz"
www_zs = "https://www.klatovynet.cz/zstolsteho"
ds_zs = "chqjjkv"