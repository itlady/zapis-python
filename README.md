# ZÁPIS do 1.tříd

## VOSPlzeň, Skriptování - python 
---

* aplikace v pythonu využívají grafického modulu tkinter
* po zvolení ZŠ aplikace umožňuje generovat oznámení obsahující nacionále školy a termín zápisu dětí do 1. tříd  a seznam dětí spadajících pod danou ZŠ
* jména dětí byla vygenerována náhodně, spádové ulice a obce byly převzaty z vyhlášky MěÚ Klatovy, které je zřizovatelem všech čtyř klatovských ZŠ
* demo data jsou v obsažena CSV databázi

