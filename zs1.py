#!/usr/bin env python3

# ZÁKLADNÍ ŠKOLA MASARYKOVA
nazev_zs = "Masarykova základní škola Klatovy"
ulice_zs = "tř. Národních mučedníků 185"
psc_zs = "339 01"
mesto_zs = "Klatovy&nbsp4"
reditel_zs = "Mgr. Emilie Salvetrová"
tel_zs = "376 312 154"
email_zs = 	"skola@maszskt.cz"
www_zs = "https://www.maszskt.cz/maszskt/"
ds_zs = "fm5f7e5"