#!/usr/bin env python3

# ZÁKLADNÍ ŠKOLA PLÁNICKÁ
nazev_zs = "Základní škola Plánická ulice"
ulice_zs = "Plánická ul. 194"
psc_zs = "339 01"
mesto_zs = "Klatovy I"
reditel_zs = "Mgr. Karel Denk"
tel_zs = "376 313 451"
email_zs = "reditel@zsklatovyplanicka.cz"
www_zs = "http://www.zsklatovyplanicka.cz"
ds_zs = "xe4yveb"